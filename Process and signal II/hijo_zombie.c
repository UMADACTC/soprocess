#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <wait.h>

/**************************************************/
int main()
{
 int pid_fork, pid_wait, status;
 int i=0;

 pid_fork=fork(); // creamos un proceso hijo
  
 if(pid_fork<0) { perror("fork"); exit(-1); }

 if(pid_fork==0)
 { // proceso hijo
  printf("Proceso hijo pid %d: ejecutando...\n",getpid());
  while(1) {sleep(1); printf("hijo: %d seg\n",++i); } // bucle de retardo
 }
 else
 { // padre

  sleep(5); // pausa de 5 segundos

  printf("\nProceso padre pid %d: mandando sennal SIGINT\n",getpid());
  kill(pid_fork,SIGINT);
  sleep(20);
  exit(0);
}
}
